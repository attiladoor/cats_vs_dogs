import os
import numpy as np
import tensorflow as tf
from datetime import datetime
from alexnet import AlexNet
from imglist import ImgList

# Path to the textfiles for the trainings and validation set
train_file = '/mnt/storage/project/cat_vs_dog/train.txt'
val_file = '/mnt/storage/project/cat_vs_dog/test.txt'
eval_file = '/mnt/storage/project/cat_vs_dog/eval.txt'

# Learning params
learning_rate = 0.01
num_epochs = 10
batch_size = 16

# Network params
dropout_rate = 0.5
num_classes = 2
train_layers = ['fc8','dropout7' 'fc7', 'dropout6', 'fc6', 'conv5']

# Path for tf.summary.FileWriter and to store model checkpoints
filewriter_path = "/tmp/finetune_alexnet/dogs_vs_cats"

# TF placeholder for graph input and output
input_tensor = tf.placeholder(tf.float32, [batch_size, 227, 227, 3])
ground_truth_tensor = tf.placeholder(tf.float32, [None, num_classes])
keep_prob = tf.placeholder(tf.float32)

# Initialize model
model = AlexNet(input_tensor, keep_prob, num_classes, train_layers)

# Link variable to model output
output_tensor = model.fc8

# List of trainable variables of the layers we want to train
var_list = [v for v in tf.trainable_variables() if v.name.split('/')[0] in train_layers]

# Op for calculating the loss
with tf.name_scope("cross_ent"):
    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=output_tensor, labels=ground_truth_tensor))

# Train op
with tf.name_scope("train"):
    # Get gradients of all trainable variables
    gradients = tf.gradients(loss, var_list)
    gradients = list(zip(gradients, var_list))

    # Create optimizer and apply gradient descent to the trainable variables
    optimizer = tf.train.GradientDescentOptimizer(learning_rate)
    train_op = optimizer.apply_gradients(grads_and_vars=gradients)

# Add the loss to summary
tf.summary.scalar('cross_entropy', loss)

# Evaluation op: Accuracy of the model
with tf.name_scope("accuracy"):
    correct_pred = tf.equal(tf.argmax(output_tensor, 1), tf.argmax(ground_truth_tensor, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

# Add the accuracy to the summary
tf.summary.scalar('accuracy', accuracy)

# Merge all summaries together
merged_summary = tf.summary.merge_all()

# Initialize the FileWriter
writer = tf.summary.FileWriter(filewriter_path)

# Initialize an saver for store model checkpoints
saver = tf.train.Saver()

# Initalize the data generator seperately for the training and validation set
train_generator = ImgList(train_file, horizontal_flip=True, shuffle=True, rotate=True, crop=10)
val_generator = ImgList(val_file, shuffle=False, rotate=False)
eval_generator = ImgList(eval_file, shuffle=False, rotate=False)
# Get the number of training/validation steps per epoch
train_batches_per_epoch = np.floor(train_generator.data_size / batch_size).astype(np.int16)
val_batches_per_epoch = np.floor(val_generator.data_size / batch_size).astype(np.int16)
eval_batches_per_epoch = np.floor(eval_generator.data_size / batch_size).astype(np.int16)

# Start Tensorflow session
with tf.Session() as sess:
    # Initialize all variables
    sess.run(tf.global_variables_initializer())

    # Add the model graph to TensorBoard
    writer.add_graph(sess.graph)

    # Load the pretrained weights into the non-trainable layer
    model.load_initial_weights(sess)

    print("{} Start training...".format(datetime.now()))
    print("{} Open Tensorboard at --logdir {}".format(datetime.now(),
                                                      filewriter_path))

    # Loop over number of epochs
    for epoch in range(num_epochs):

        print("{} Epoch number: {}".format(datetime.now(), epoch + 1))

        training_step = 1

        while training_step < train_batches_per_epoch:

            # Get a batch of images and labels
            input_batch, ground_truth, _ = train_generator.generate_next_batch(batch_size)

            # And run the training op
            sess.run(train_op, feed_dict={input_tensor: input_batch,
                                          ground_truth_tensor: ground_truth, keep_prob: dropout_rate})

            # Generate summary with the current batch of data and write to file
            s = sess.run(merged_summary, feed_dict={input_tensor: input_batch,
                                                    ground_truth_tensor: ground_truth, keep_prob: 1.})
            writer.add_summary(s, epoch * train_batches_per_epoch + training_step)

            training_step += 1

        # Validate the model on the entire validation set
        print("{} Start validation".format(datetime.now()))
        test_acc = 0.
        test_count = 0
        for _ in range(val_batches_per_epoch):
            test_input_batch, test_ground_truth, _ = val_generator.generate_next_batch(batch_size)
            test_accuracy = sess.run(accuracy, feed_dict={input_tensor: test_input_batch,
                                                ground_truth_tensor: test_ground_truth, keep_prob: 1.})
            test_acc += test_accuracy
            test_count += 1
        test_acc /= test_count
        print("{} Validation Accuracy = {:.4f}".format(datetime.now(), test_acc))

        # Reset the file pointer of the image data generator
        val_generator.reset_list()
        train_generator.reset_list()
    f = open('results.csv', 'w')

    eval_generator.reset_list()
    print("{} evaluation {}".format(datetime.now(), eval_batches_per_epoch ))
    for x in range(eval_batches_per_epoch):
        results = []
        eval_input_batch, _, names = eval_generator.generate_next_batch(batch_size)
        output_values = sess.run(model.fc8, feed_dict={input_tensor: eval_input_batch,
                                               keep_prob: 1.})

        for i in range(len(output_values)):
            max_index = output_values[i].tolist().index(max(output_values[i]))
            results.append(max_index)
            with open('results.csv', 'a') as f:
                name = names[i].split('.')[0]
                f.write(name + "," + str(max_index) + "\n")


