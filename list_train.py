import os
import sys

f = open("train.txt","w")
f.close()

DIR = sys.argv[1]
LOCATION = '/mnt/storage/project/cat_vs_dog/'


for file in os.listdir(DIR):
	if file[0:3] == "cat":
		with open("train.txt","a") as f:
			f.write(os.path.join(LOCATION, DIR, file) + " 0\n")

	if file[0:3] == "dog":
                with open("train.txt","a") as f:
                        f.write(os.path.join(LOCATION, DIR, file) + " 1\n")


