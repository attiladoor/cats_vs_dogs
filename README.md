# Fine tuning solution for Cats vs Dogs machine learning challenge
Cats vs Dogs is a really wide known image processing and classification challange. Here you can find further informations: https://www.kaggle.com/

## Architecture
This neural network is based on AlexNet CCN 
![alt text](https://www.researchgate.net/profile/Santosh_Ravi_Kiran_Sarvadevabhatla/publication/289928157/figure/fig7/AS:318276145041408@1452894354477/Figure-1-An-illustration-of-the-weights-in-the-AlexNet-model-Note-that-after-every.png)

But due to the small number of training datas (~25000 images), i decided to use fine tuning.  In order to perform fine tuning we need a saved graph without being converted to constants. Here you can find the required file (bvlc_alexnet.npy) http://www.cs.toronto.edu/~guerzhoy/tf_alexnet/

Furthermore here is a pretty handy sample code in Tensorflow, about building up AlexNet 

## Dataset
If you register to the website of the challenge, you can download the training images, what you would need for working around the task. After downloading, you will have many images with the following sample names:
>
dog.4127.jpg
>
cat.277.jpg
>
dog.7831.jpg
>
dog.5450.jpg
>
cat.3986.jpg
>
dog.1778.jpg

## Generating training dataset list:
The **finetune.py** requires at least one text file which includes the path of images, and the belonging class. In order to generate our your own dataset list, use the **list_train.py** with one argument what is the folder of your training images:
>python3 list_train.py train_images_dir

And then you should get a **train.txt** file, with the following content:
>
/mnt/storage/project/cat_vs_dog/train/dog.4127.jpg 1
>
/mnt/storage/project/cat_vs_dog/train/cat.277.jpg 0
>
/mnt/storage/project/cat_vs_dog/train/dog.7831.jpg 1
>
/mnt/storage/project/cat_vs_dog/train/dog.5450.jpg 1
>
/mnt/storage/project/cat_vs_dog/train/cat.3986.jpg 0

It is supposed to have 25000 lines, and if you cut out few line (~1000) and add it to a **test.txt** you can easily get a test dataset. (but this is optional) If you decide to not use evaluation, or testing, you have to create the regarding file anyway.

## Generating evaluation dataset list
To get the proper CSV file for the evaluation of the challenge, you need a to run the **list_eval.py** in the same way as the list_train.py, so with the folder of the evaluation images
>python3 list_train.py eval_images_dir

After running this script, you should get a **eval.txt**, which is supposed to be very similar to train.txt, but it is without class numbers

After all this, you are ready to run **finetune.py** just you should set the path of your dataset list files.
> 
#Path to the textfiles for the trainings and validation set
>
train_file = '/mnt/storage/project/cat_vs_dog/train.txt'
>
val_file = '/mnt/storage/project/cat_vs_dog/train.txt'
>
eval_file = '/mnt/storage/project/cat_vs_dog/eval.txt'

And the desired training image distortions in order of data augmentation
>train_generator = ImgList(train_file, horizontal_flip=[boolean], shuffle=[boolean], rotate=[boolean], crop=[%])

## Results
After running my program code, and using 1000 images for testing, with the following settings:
```python
#Learning params
learning_rate = 0.01
num_epochs = 9
batch_size = 16
#Network params
dropout_rate = 0.5
num_classes = 2
train_layers = ['fc8','dropout7' 'fc7', 'dropout6', 'fc6', 'conv5']
train_generator = ImgList(train_file, horizontal_flip=True, shuffle=True, rotate=True, crop=10)
```
I got the following accuracy measurement result:

```python
2017-05-24 18:59:29.755634 Start training...
2017-05-24 18:59:29.755688 Open Tensorboard at --logdir /tmp/finetune_alexnet/dogs_vs_cats
2017-05-24 18:59:29.755701 Epoch number: 1
2017-05-24 19:05:22.581862 Start validation
2017-05-24 19:09:00.732031 Validation Accuracy = 0.9150
2017-05-24 19:09:00.741639 Epoch number: 2
2017-05-24 19:15:01.311258 Start validation
2017-05-24 19:18:39.960226 Validation Accuracy = 0.9588
2017-05-24 19:18:39.970006 Epoch number: 3
2017-05-24 19:24:33.427060 Start validation
2017-05-24 19:28:11.269614 Validation Accuracy = 0.9721
2017-05-24 19:28:11.279295 Epoch number: 4
2017-05-24 19:34:05.655344 Start validation
2017-05-24 19:37:45.033392 Validation Accuracy = 0.9882
2017-05-24 19:37:45.041003 Epoch number: 5
2017-05-24 19:43:41.007596 Start validation
2017-05-24 19:47:20.547344 Validation Accuracy = 0.9889
2017-05-24 19:47:20.557007 Epoch number: 6
2017-05-24 19:53:16.793528 Start validation
2017-05-24 19:56:55.831926 Validation Accuracy = 0.9950
2017-05-24 19:56:55.841768 Epoch number: 7
2017-05-24 20:02:52.159481 Start validation
2017-05-24 20:06:31.277770 Validation Accuracy = 0.9976
2017-05-24 20:06:31.287639 Epoch number: 8
2017-05-24 20:12:27.305842 Start validation
2017-05-24 20:16:06.596295 Validation Accuracy = 0.9975
2017-05-24 20:16:06.606480 Epoch number: 9
2017-05-24 20:22:02.477489 Start validation
2017-05-24 20:25:41.840210 Validation Accuracy = 0.9982
```

To se my evaluation CSV, check out the results.csv
